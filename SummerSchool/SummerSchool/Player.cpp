#include "stdafx.h"
#include "Player.h"
#include <cstring>
#include<iostream>



Player::Player(const char * name) : m_name(nullptr), m_hand()
{
	m_name = new char[strlen(name) + 1]; // add 1 for '\0'
	strcpy(m_name, name);
}

Player::~Player()
{
	delete[] m_name;
	m_name = nullptr;
}

const char * Player::GetName() const
{
	return m_name;
}

Hand & Player::GetHand()
{
	return m_hand;
}

Action Player::GetPlayerAction() const
{
	Action returnValue = STAND;
	std::cout << "What would you like to do? Press 1 for HIT or 2 for STAND";
	std::cin >> returnValue;

	return returnValue;
}
