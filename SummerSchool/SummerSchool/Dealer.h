#pragma once
#include "Deck.h"
#include "Shoe.h"
#include "Player.h"

class Player;
class Dealer : public Player
{
public:
	Dealer();
	~Dealer();
	Dealer(const char * name);
    
	void Shuffle();
	void Deal(Player* player);
    Action GetAction() const override ;
private:
	Deck m_deck;
	Shoe m_shoe;

};

