// SummerSchool.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Player.h"
#include "Game.h"

int main()
{
	Player* players[] =
	{
		new Player("Player 1"),
		new Player("Player 2")
	};
	size_t numPlayers = sizeof(players) / sizeof(Player*);

	Game game(numPlayers, players);

	game.Play();

	for (Player* player : players)
	{
		delete player;
		player = nullptr;
	}
	
    return 0;
}

