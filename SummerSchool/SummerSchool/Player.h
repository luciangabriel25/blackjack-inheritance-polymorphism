#pragma once
#include "Hand.h"

enum Action
{
	HIT,
	STAND
};

class Player
{
public:
	Player(const char* name);
	~Player();
    Player();

	const char* GetName() const;
	Hand& GetHand();
	Action GetPlayerAction();
	virtual Action GetAction() const;


protected:
	Hand m_hand;
	char* m_name;
};

